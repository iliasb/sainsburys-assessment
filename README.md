## Overview

This repo is for Sainsbury's DevOps technical test.

## Setup


### Requirements

The following are required to get this project working:
  - Ruby > 1.9.3 with rubygems ( tested with 2.0.0 )
  - Ruby headers and gcc in case of Gems that need to be natively compiled
  - Bundler Gem
  - Vagrant 1.6.x ( tested with 1.8.0 )
  - Virtualbox 5.0 ( tested under Mac OS X 10.11 El Capitan )
 

### Usage

## Starting it up for the first time

You can get everything working by running `start.sh`.

Which will do the following for you:
  - Run `bundle install` to install all the Gems with the versions pinned in `Gemfile.lock`
  - Run `bundle exec librarian-puppet` to fetch and install all the puppet modules with the versions pinned in `Puppetfile.lock`
  - Run `vagrant up` which will start up 3 nodes, 1 webserver, 2 application servers and provision them with puppet.

Once all of the above are complete you can hit the webserver on `http://10.100.10.2` to access the 2 application servers backends.


## Changing the Go app and deploying it

The Go app is located in `localmodules/golang_app/files/app.go` and once modified it can be redeployed to the application servers by running `deploy.sh`


