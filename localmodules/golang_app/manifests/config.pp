class golang_app::config (
  $app_filename,
  $deploy_dir,
  $service_name,
) inherits ::golang_app::params {

  file { "/usr/lib/systemd/system/${service_name}.service":
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('golang_app/app_service.erb')
  }


}