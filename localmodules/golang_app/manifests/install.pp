class golang_app::install (
  $app_filename,
  $deploy_dir,
) inherits ::golang_app::params {
  
  exec { 'app_dir':
    command => 'mkdir -p /usr/local/src/app',
    path => '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin',
    creates => '/usr/local/src/app', 
  } ->
  file { 'app':
    ensure => present,
    path => '/usr/local/src/app/main.go',
    source => 'puppet:///modules/golang_app/app.go', 
  } ~>
  exec { 'build_app':
    command => "go build -o ${app_filename}",
    cwd => '/usr/local/src/app',
    path => '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:/usr/local/go/bin',
    refreshonly => true,
  } ~>
  exec { 'install_app':
    command => "cp -f ${app_filename} ${deploy_dir}",
    cwd => '/usr/local/src/app',
    path => '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin',
    refreshonly => true,
  }



}