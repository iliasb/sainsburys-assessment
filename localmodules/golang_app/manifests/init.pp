class golang_app (
  $service_name = $golang_app::params::service_name,
  $deploy_dir = $golang_app::params::deploy_dir,
  $app_filename = $golang_app::params::app_filename,
) inherits ::golang_app::params {
  
  
  class { '::golang_app::install':
    deploy_dir   => $deploy_dir,
    app_filename => $app_filename,
  } ->
  class { '::golang_app::config': 
    service_name => $service_name,
    deploy_dir   => $deploy_dir,
    app_filename => $app_filename,
  } ~>
  class { '::golang_app::service': 
    service_name => $service_name,
  } ->
  Class['::golang_app']

  Class['::golang_app::install']~>Class['::golang_app::service']


}