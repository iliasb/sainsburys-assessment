class golang_app::service (
  $service_name,
) inherits ::golang_app::params {
  

  service { $service_name:
    ensure     => running,
    enable     => true,
    hasstatus  => false,
    hasrestart => false,
  }


}