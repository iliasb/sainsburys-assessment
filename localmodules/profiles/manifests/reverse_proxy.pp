class profiles::reverse_proxy {
  
  include nginx

  Class['nginx'] ->
  firewall { '100 allow webserver http access':
    dport   => '80',
    proto  => tcp,
    action => accept,
  }

}