class profiles::app {

  $params = hiera_hash('go_app', {})

  include golang

  Class['golang']->
  class { 'golang_app':
    service_name => $params[service_name],
    app_filename => $params[app_filename],
    deploy_dir   => $params[deploy_dir],
  } ->
  # Not the cleanest way but it is not worth it to over-engineer it
  firewall { '100 allow golang app access':
    dport   => '8484',
    proto  => tcp,
    action => accept,
  } 


}