#!/bin/bash

bundle install

if [ $? -ne 0 ]
 then 
  echo "Error in bundle install."
  exit 1
fi

bundle exec librarian-puppet install

if [ $? -ne 0 ]
 then 
  echo "Error in librarian-puppet install."
  exit 1
fi

vagrant up webserver

if [ $? -ne 0 ]
 then  
  echo "Error in vagrant up of webserver."
  exit 1
fi

vagrant up app1

if [ $? -ne 0 ]
 then 
  echo "Error in vagrant up of appserver 1."
  exit 1
fi

vagrant up app2

if [ $? -ne 0 ]
 then 
  echo "Error in vagrant up of appserver 2."
  exit 1
fi


echo "All completed, you can now access the 2 app server in round-robin via http://10.100.10.2"